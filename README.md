# MulticastClient

An arduino library to easily send data to an IoT MulticastCloud server like [api.kast.cf](http://api.kast.cf).

### Dependencies

It depends on [ArduinoJson](https://github.com/bblanchon/ArduinoJson) library and any of the among clients: [WifiClient](https://www.arduino.cc/en/Reference/WiFiClient), [EthernetClient](https://www.arduino.cc/en/Reference/EthernetClient) or [GSMClient](https://www.arduino.cc/en/Tutorial/GSMExamplesWebClient).

### Basic use

```c

WiFiClient client;
MulticastClient multicast = MulticastClient(&client, "<sensorKey>", "<streamKey>");
int statusCode = multicast.postData(<unix timestamp>, <value>);

```
