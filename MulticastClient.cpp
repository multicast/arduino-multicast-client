#include "MulticastClient.h"

#define HEADER_AUTH_TOKEN "x-auth-token"

MulticastClient::MulticastClient(Client* _client, const char* _host) {
  rest =  new RestClient(_client, _host);
}

MulticastClient::MulticastClient(Client* _client, const char* _host, int _port) {
  rest = new RestClient(_client, _host, _port);
}

MulticastClient::MulticastClient(Client* _client, const char* _sensorKey, const char* _streamKey) {
    rest = new RestClient(_client, __DEFAULT_MULTICAST_ENDPOINT);
    sensorKey = _sensorKey;
    streamKey = _streamKey;
}

MulticastClient::MulticastClient(Client* _client, const char* _sensorKey, const char* _streamKey,
  const char* _xauthtoken) {
    rest = new RestClient(_client, __DEFAULT_MULTICAST_ENDPOINT);
    sensorKey = _sensorKey;
    streamKey = _streamKey;
    xauthtoken = _xauthtoken;
    rest->setHeader("x-auth-token", xauthtoken);
}

void MulticastClient::MultCastSetHost_Port(const char* _host, int _port){
  rest->setHostPort(_host, _port);
}


MulticastClient::~MulticastClient() {
  delete rest;
}

int MulticastClient::measurePath(const char* _sensorKey, const char* _streamKey) {
    return (strlen(__SENSOR_RES_PATH) + strlen(__STREAM_RES_PATH) + strlen(_sensorKey) + strlen(_streamKey) + 4) + 1;
}

char* MulticastClient::buildPath(const char* _sensorKey, const char* _streamKey, char* _path) {
  strcat(_path, "/");
  strcat(_path, __SENSOR_RES_PATH);
  strcat(_path, "/");
  strcat(_path, sensorKey);
  strcat(_path, "/");
  strcat(_path, __STREAM_RES_PATH);
  strcat(_path, "/");
  strcat(_path, streamKey);
  strcat(_path,"/data");
  strcat(_path, '\0');
}

int MulticastClient::postData(unsigned long timestamp, float value) {

    const int JSON_BUFFER = JSON_OBJECT_SIZE(4);
    StaticJsonBuffer<JSON_BUFFER> jsonBuffer;
    JsonObject& lJson = jsonBuffer.createObject();
    lJson["timestamp"] = timestamp*1000;
    JsonObject& jsonValues = lJson.createNestedObject("values");
    jsonValues["temp"] = value;
    // jsonValues["humi"] = value;  // to si71 sensor
    // lJson["value"] = value;      //  old ways.

    char* lContent = (char*) malloc(lJson.measureLength() + 1);
    lJson.printTo(lContent, lJson.measureLength() + 1);
    lContent[lJson.measureLength()] = '\0';

    // Serial.println(lContent);

    char lPath[96];
    lPath[0] = '\0';
    buildPath(sensorKey, streamKey, lPath);
    //Serial.print("lPath:");
    //Serial.println(lPath);
    // rest->setHeader(HEADER_AUTH_TOKEN, xauthtoken);
    // Serial.println("lContent:");
    // Serial.print(lContent);
    int statusCode = rest->put(lPath, lContent, NULL);

    free(lContent);

    return statusCode;

}
