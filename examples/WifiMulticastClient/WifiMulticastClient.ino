/*
* arduino code targeted to arduino board and wifi shield, developed on Arduino IDE 1.6.7.
*
* This code get the environment temperatura through temperature sensor DS18b20,
*  for that use the <DS1820> library. Once the temperature is obtained it's
*  sended with the timestamp to the server in json format through the <MulticastClient>
*  library.
*
* - If no sensor is detected, no data will be send.
* - The USER_KEY, SENSOR_KEY and SENSOR_KEY_STREAM must be defined and valid on the server.
* - The server's url and port must be defined, or de code will use default values.
*/


#include <SPI.h>
#include <WiFi.h>
#include "MulticastClient.h"
#include <ArduinoJson.h>
#include <Time.h>

#include <OneWire.h>
#include "DS1820.h"


// =============== timers, pins =====================
#define pinSensor 8 // pin in which the temperature sensor is connected
#define numDS 1     // amount of sensor will be connected to the pin
#define TIMER_ENVIO 30000 // interval between data submissions

//========================== user token ====================
#define USER_KEY "cba991ae0c38408a84cf01687c60d075"

//=============== Sensors Key , Stream Key ==================
// #define  SENSOR_KEY "9887c50623bd4584bd106d37d84d6bcc"           // Sensor 001
// #define  SENSOR_KEY_STREAM "985bf2cde9b54a54b8fcd3423d89ad89"   // Sensor 001 stream

#define  SENSOR_KEY "5fd52f447a2f453eaf73723e46759cc3"           // Sensor 002
#define  SENSOR_KEY_STREAM "438bdfea6d254e3d873292de2da285dd"   // Sensor 002 stream




// #define  SENSOR_KEY "fopsqeh8mk"           // Sensor 001
// #define  SENSOR_KEY_STREAM "2ohof2uphv"   // Sensor 001 stream
// #define  SENSOR_KEY "95fc5k9221"                //sensor1_eth
// #define  SENSOR_KEY_STREAM "i2o9gtgj85"         // stream
// #define  SENSOR_KEY "ctkciipm8d"        // Sensor 002
// #define  SENSOR_KEY_STREAM "oajruihll1"        // Sensor 002 stream
// #define  SENSOR_KEY "9cesk0sv8k"        // Sensor 003
// #define  SENSOR_KEY_STREAM "ohlctehis5"        // Sensor 003 stream
// #define  SENSOR_KEY "caovqqr8vu"        // Sensor 004
// #define  SENSOR_KEY_STREAM "89od0md3hj"        // Sensor 004 stream


/*
"oid":4,"key":"9887c50623bd4584bd106d37d84d6bcc","name":"wifi001","description":"wifi001"
"streams":[{"oid":6,"key":"985bf2cde9b54a54b8fcd3423d89ad89","name":"temperature"}]

"oid":5,"key":"5fd52f447a2f453eaf73723e46759cc3","name":"wifi002","description":"wifi002",
"streams":[{"oid":7,"key":"438bdfea6d254e3d873292de2da285dd","name":"temperature"}]

"oid":6,"key":"c0b79ec266af4c01acc33a9b84240eb7","name":"eth003","description":"eth003"
"streams":[{"oid":8,"key":"96ad88fd59a044bfa3929e243da383bc","name":"temperature"}]

"oid":7,"key":"878d1cc354cc40e795e99e40e9fe9e8a","name":"eth004","description":"eth004","owner"
"streams":[{"oid":9,"key":"f9e75e128e33449285e7f83b04fab6dd","name":"temperature"}]

"oid":8,"key":"25cc1a5f2b1f4ccd9c5d5d5cd39ffb7b","name":"eth005","description":"eth005"
"streams":[{"oid":10,"key":"86763252b78f4bc386e4e4f933e5b352","name":"temperature"}]
*/

/* ===================== functions Prototypes ======================= */
void printWifiStatus(); // Prints on serial network information
unsigned long getTime(); // get the timestamp
int sendData();          // send data to the server
void manageConnection(); // manage the wifi connection
void cleanJsonString(char* _str, char* _result); // clean the server response leaving only a json string

/* ========= connection data ============= */
char ssid[] = "GVT-3D89";      //  your network SSID (name)
char pass[] = "S1EB579864";     // your network password

/*================== host ======================*/
static const char* dataHost = "multicast.vix.br";


// Initialize the Wifi client library
WiFiClient client;
unsigned int CHECK_CONNECTION = 0;
// initialize parameters of the sensor
// DS1820 <name>(pin_of_sensor, quantity_of_sensor);
DS1820 mySensors(8,1);

// Get the timestamp from server
unsigned long getTime() {
  // Serial.println(F("getting time from server..."));
  RestClient rest = RestClient(&client, "api.timezonedb.com");
  String response;
  int statusCode = rest.get("/?zone=America/Sao_Paulo&key=0XB1Z06VAUM2&format=json", &response);
  char* buffer = (char*) malloc(response.length()*sizeof(char));
  response.toCharArray(buffer, response.length());
  char* result = (char*) malloc(156*sizeof(char));
  result[0] = '\0';
  cleanJsonString(buffer, result);
  free(buffer);
  const int JSON_BUFFER = JSON_OBJECT_SIZE(8);
  StaticJsonBuffer<JSON_BUFFER> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(result);
  free(result);
  unsigned long temp = root[F("timestamp")];
  // Serial.print("timestamp: ");
  Serial.println(temp);
  return temp;
}
// Clean the server response leaving only the json string
void cleanJsonString(char* str, char* result) {
  int len = strlen(str) - 1;
  int start, finish, i;
  for (i = 0; str[i] != '{'; i++ );
  start = i;
  for (i = len; str[i] != '}'; i-- );
  finish = i;
  int j = 0;
  for (i = start; i <= finish; i++) {
    result[j] = str[i];
    j++;
  }
  result[j] = '\0';
}


void setup() {

   Serial.begin(115200);
	delay(3000);
   // check for the presence of the shield:
 if (WiFi.status() == WL_NO_SHIELD) {
   Serial.println("WiFi shield not present");
   // don't continue:
   while (true);
 }
delay(1000);
 String fv = WiFi.firmwareVersion();
 if (fv != "1.1.0") {
   Serial.println("Please upgrade the firmware");
 }

  // attempt to connect to Wifi network:
byte tryCt=0;
while (WiFi.status() != WL_CONNECTED && tryCt < 20) { // to prevent stuk on this loop
   Serial.print("#");
   // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
   WiFi.begin(ssid, pass);
   // wait 10 seconds for connection:
   delay(100);
   tryCt++;
 }
 Serial.print("\n");
  // you're connected now, so print out the status:
 printWifiStatus();
 setTime(getTime());
 
}

void loop() {
  sendData();
  manageConnection();
}

/*
* Wait a amount of time <TIMER_ENVIO>, read the temperature sensor, timestamp
* and send it to the server.
*/
int sendData()
{
  static unsigned long lastExec = 0;
  if( (millis() - lastExec) > TIMER_ENVIO){
  lastExec  = millis();
   MulticastClient multicast  = MulticastClient(&client, SENSOR_KEY,
      SENSOR_KEY_STREAM,USER_KEY);
  /*
  * Set the host and port that will receive data. If not seted the libray will
  * use default host defined inside MulticastClient.h file and port 80.
  */
   multicast.MultCastSetHost_Port(dataHost, 8082);
  /*
  *  for 1 sensor on bus.
  *  getTemperature() return the integer value of the temperature * 10.
  *  ex:    room temperature 22,5C will be 225;
  *
  */
  float value = mySensors.getTemperature()/10.0;
  /*
  * if the address is 0 there was a failure in the reading process or the sensor * was not found, so don't send data to the server.
  */
    if(mySensors.getAddressSensor() != 0) {
      int statusCode = multicast.postData(now(), value);
      return statusCode;
    } else Serial.println("no sensor");
  }
}
//
