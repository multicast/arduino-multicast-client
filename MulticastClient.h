#ifndef MulticastClient_h
#define MulticastClient_h

#include "ArduinoJson.h"
#include "RestClient.h"

static const char* __SENSOR_RES_PATH = "sensors";
static const char* __STREAM_RES_PATH = "streams";
//static const char* __DEFAULT_MULTICAST_ENDPOINT = "162.243.237.230";
static const char* __DEFAULT_MULTICAST_ENDPOINT = "multicast.vix.br";

class MulticastClient {

  public:
    MulticastClient(Client* _client, const char* _sensorKey, const char* _streamKey);
    MulticastClient(Client* _client, const char* _sensorKey, const char* _streamKey,
                    const char* _xauthtoken);
    MulticastClient(Client* _client, const char* _host);
    MulticastClient(Client* _client, const char* _host, int _port);
    ~MulticastClient();
    void MultCastSetHost_Port(const char* _host, int _port);
    
    int postData(unsigned long timestamp, float value);


  private:
    RestClient* rest;
    const char* sensorKey;
    const char* streamKey;
    const char* xauthtoken;
    char* buildPath(const char* _sensorKey, const char* _streamKey, char* _path);
    int measurePath(const char* _sensorKey, const char* streamKey);

};
#endif
